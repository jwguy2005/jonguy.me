using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DomainEvents.Interface
{
    public class EventHub
    {
        private readonly List<IWeakAction> _handlers = new List<IWeakAction>();

        public IDisposable Subscribe<T>(Action<T> action)
        {
            var weakAction = new WeakAction<T>(action);
            _handlers.Add(weakAction);

            // Will this leak?  Closure will hold a reference to my _handlers, so my lifetime
            // becomes the length of whoever is holding disposer.  Probably not an issue since
            // EventHub would generally be a singleton.
            return new Disposer(() => Unsubscribe(weakAction));
        }

        public void Publish<T>(T @event)
        {
            if (@event == null) throw new ArgumentNullException("@event");

            var eventType = typeof (T);
            foreach (var handler in _handlers)
            {
                if (handler.ParameterType.IsAssignableFrom(eventType))
                {
                    handler.Invoke(@event);
                }
            }
        }

        public void Publish<T>() where T : new()
        {
            var t = new T();
            Publish(t);
        }

        private void Unsubscribe(IWeakAction weakAction)
        {
            // go ahead and try to cleanup any WeakReferences whose target
            // has been GCed
            _handlers.RemoveAll(h => h == weakAction || !h.IsAlive);
        }
    }

    public class EventHub<TEventBase>
    {
        private readonly Dictionary<IWeakAction, Type> _handlers = new Dictionary<IWeakAction, Type>();

        public IDisposable Subscribe<TEvent>(Action<TEvent> action) where TEvent : TEventBase
        {
            var weakAction = new WeakAction<TEvent>(action);
            _handlers.Add(weakAction, typeof (TEvent));

            // Will this leak?  Closure will hold a reference to my _handlers, so my lifetime
            // becomes the length of whoever is holding disposer.  Probably not an issue since
            // EventHub would generally be a singleton.
            return new Disposer(() => _handlers.Remove(weakAction));
        }

        public void Publish<TEvent>(TEvent @event) where TEvent : TEventBase
        {
            if (@event == null) throw new ArgumentNullException("@event");

            var eventType = typeof (TEvent);
            foreach (var kvp in _handlers.Where(kvp => kvp.Value.IsAssignableFrom(eventType)))
            {
                kvp.Key.Invoke(@event);
            }
        }

        public void Publish<TEvent>() where TEvent : TEventBase, new()
        {
            var t = new TEvent();
            Publish(t);
        }
    }

    internal class Disposer : IDisposable
    {
        private readonly Action _action;

        public Disposer(Action action)
        {
            this._action = action;
        }

        public void Dispose()
        {
            _action();
        }
    }

    internal interface IWeakAction
    {
        void Invoke(object parameter);
        bool IsAlive { get; }
        Type ParameterType { get; }
    }

    internal class WeakAction<T> : IWeakAction
    {
        private readonly Type _delegateType;
        private readonly MethodInfo _methodInfo;
        private readonly WeakReference _weakReference;

        public WeakAction(Action<T> action)
        {
            // ! Hold a weak reference to the target of the delegate, not the delegate itself
            this._weakReference = new WeakReference(action.Target);

            // Remember the method and type of the delegate so that it can be invoked later
            this._methodInfo = action.Method;
            this._delegateType = action.GetType();
        }

        void IWeakAction.Invoke(object parameter)
        {
            Invoke((T) parameter);
        }

        bool IWeakAction.IsAlive
        {
            get { return _weakReference.IsAlive; }
        }

        Type IWeakAction.ParameterType
        {
            get { return typeof(T); }
        }

        private void Invoke(T parameter)
        {
            var target = _weakReference.Target;
            if (target != null)
            {
                Action<T> action;
                if (_methodInfo.IsStatic)
                {
                    action = (Action<T>) Delegate.CreateDelegate(_delegateType, null, _methodInfo);
                }
                else
                {
                    action = (Action<T>) Delegate.CreateDelegate(_delegateType, target, _methodInfo);
                }

                action.Invoke(parameter);
            }
        }
    }
}
