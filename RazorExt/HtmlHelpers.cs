using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.IO;
using HtmlBuilders;

namespace Reynolds.Cws.CompanyPortal.Web.Extensions
{
    public static class HtmlHelpers
    {
        public static Editor<T> BuildEditorFor<T>(this HtmlHelper helper, T model)
        {
            return new Editor<T>(model, helper);
        }

        public static View<T> BuildViewFor<T>(this HtmlHelper helper, T model)
        {
            return new View<T>(model, helper);
        }
    }

    public class Editor
    {
        public static string DefaultContainerFormat = "<div>{0}</div>";
        public static string DefaultChildMemberFormat = "<div class=\"editor-label\">{0}</div><div class=\"editor-field\">{1} {2}</div>";
    }

    public class Editor<TModel> : Editor, IHtmlString
    {
        private readonly TModel _model;
        private readonly HtmlHelper _html;
        private readonly List<string> _exclusions = new List<string>();
        private readonly Dictionary<string, Action<IHtmlNodeSet>> _htmlAlterations = new Dictionary<string, Action<IHtmlNodeSet>>();
        private readonly List<Tuple<Func<ModelMetadata, bool>, Action<IHtmlNodeSet>>> _conditionalHtmlAlterations = new List<Tuple<Func<ModelMetadata, bool>, Action<IHtmlNodeSet>>>();
        private readonly Dictionary<string, Action<ModelMetadata>> _metadataAlterations = new Dictionary<string, Action<ModelMetadata>>();
        private readonly List<Tuple<Func<ModelMetadata, bool>, Action<ModelMetadata>>> _conditionalMetadataAlterations = new List<Tuple<Func<ModelMetadata, bool>, Action<ModelMetadata>>>();
        private string _containerFormat;
        private string _childMemberFormat;
        
        public Editor(TModel model, HtmlHelper html)
        {
            this._model = model;
            this._html = html;
            this._containerFormat = DefaultContainerFormat;
            this._childMemberFormat = DefaultChildMemberFormat;
        }

        public Editor<TModel> SetContainerFormat(string format)
        {
            _containerFormat = format;
            return this;
        }

        public Editor<TModel> SetChildMemberFormat(string format)
        {
            _childMemberFormat = format;
            return this;
        }

        public Editor<TModel> AlterHtmlFor<T>(Expression<Func<TModel, T>> expression, Action<IHtmlNodeSet> alterRendering)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            _htmlAlterations[propertyName] = alterRendering;
            return this;
        }

        public Editor<TModel> AlterHtmlWhere(Func<ModelMetadata, bool> predicate, Action<IHtmlNodeSet> alterRendering)
        {
            _conditionalHtmlAlterations.Add(new Tuple<Func<ModelMetadata, bool>, Action<IHtmlNodeSet>>(predicate, alterRendering));
            return this;
        }

        public Editor<TModel> AlterMetadataFor<T>(Expression<Func<TModel, T>> expression, Action<ModelMetadata> alterMetadata)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            _metadataAlterations[propertyName] = alterMetadata;
            return this;
        }

        public Editor<TModel> AlterMetadataWhere(Func<ModelMetadata, bool> predicate, Action<ModelMetadata> alterMetadata)
        {
            _conditionalMetadataAlterations.Add(new Tuple<Func<ModelMetadata, bool>, Action<ModelMetadata>>(predicate, alterMetadata));
            return this;
        }

        public string ToHtmlString()
        {
            var modelMetadata = _html.ViewData.ModelMetadata ?? ModelMetadataProviders.Current.GetMetadataForType(() => _model, typeof(TModel));

            using (var writer = new StringWriter())
            {
                foreach (var prop in modelMetadata.Properties)
                {
                    if (_metadataAlterations.ContainsKey(prop.PropertyName))
                    {
                        _metadataAlterations[prop.PropertyName](prop);
                    }
                    
                    foreach (var conditional in _conditionalMetadataAlterations)
                    {
                        if (conditional.Item1(prop))
                        {
                            conditional.Item2(prop);
                        }
                    }
                    
                    if (prop.ShowForEdit)
                    {
                        var htmlAlterations = new List<Action<IHtmlNodeSet>>();

                        if (_htmlAlterations.ContainsKey(prop.PropertyName))
                        {
                            htmlAlterations.Add(_htmlAlterations[prop.PropertyName]);
                        }

                        foreach (var conditional in _conditionalHtmlAlterations)
                        {
                            if (conditional.Item1(prop))
                            {
                                htmlAlterations.Add(conditional.Item2);
                            }
                        }

                        var htmlString = string.Format(_childMemberFormat,
                            System.Web.Mvc.Html.LabelExtensions.Label(_html, prop.PropertyName),
                            System.Web.Mvc.Html.EditorExtensions.Editor(_html, prop.PropertyName),
                            System.Web.Mvc.Html.ValidationExtensions.ValidationMessage(_html, prop.PropertyName));

                        var doc = new HtmlAgilityPack.HtmlDocument();
                        doc.LoadHtml(htmlString);
                        var htmlNode = new HtmlNode(doc.DocumentNode);

                        foreach (var alteration in htmlAlterations)
                        {
                            alteration(htmlNode);
                        }

                        writer.Write(htmlNode.InnerHtml);

                        if (prop.AdditionalValues.ContainsKey("Confirm") && (bool)prop.AdditionalValues["Confirm"])
                        {
                            if (_html.ViewData.ModelState.ContainsKey(prop.PropertyName) && _html.ViewData.ModelState[prop.PropertyName].Errors.Any())
                            {
                                _html.ViewData.ModelState.Remove(prop.PropertyName);
                            }

                            htmlString = string.Format(_childMemberFormat,
                                "Confirm " + prop.GetDisplayName(),
                                System.Web.Mvc.Html.EditorExtensions.Editor(_html, prop.PropertyName),
                                "");

                            doc = new HtmlAgilityPack.HtmlDocument();
                            doc.LoadHtml(htmlString);
                            htmlNode = new HtmlNode(doc.DocumentNode);

                            foreach (var alteration in htmlAlterations)
                            {
                                alteration(htmlNode);
                            }

                            writer.Write(htmlNode.InnerHtml);
                        }
                    }
                }
                
                return String.Format(_containerFormat, writer.ToString());
            }
        }
    }

    public class View
    {
        public static string DefaultContainerFormat = "<div>{0}</div>";
        public static string DefaultChildMemberFormat = "<div class=\"display-label\">{0}</div><div class=\"display-field\">{1}</div>";
    }

    public class View<TModel> : View, IHtmlString
    {
        private readonly TModel _model;
        private readonly HtmlHelper _html;
        private readonly List<string> _exclusions = new List<string>();
        private readonly Dictionary<string, Action<IHtmlNodeSet>> _htmlAlterations = new Dictionary<string, Action<IHtmlNodeSet>>();
        private readonly List<Tuple<Func<ModelMetadata, bool>, Action<IHtmlNodeSet>>> _conditionalHtmlAlterations = new List<Tuple<Func<ModelMetadata, bool>, Action<IHtmlNodeSet>>>();
        private readonly Dictionary<string, Action<ModelMetadata>> _metadataAlterations = new Dictionary<string, Action<ModelMetadata>>();
        private readonly List<Tuple<Func<ModelMetadata, bool>, Action<ModelMetadata>>> _conditionalMetadataAlterations = new List<Tuple<Func<ModelMetadata, bool>, Action<ModelMetadata>>>();
        private string _containerFormat;
        private string _childMemberFormat;

        public View(TModel model, HtmlHelper html)
        {
            this._model = model;
            this._html = html;
            this._containerFormat = DefaultContainerFormat;
            this._childMemberFormat = DefaultChildMemberFormat;
        }

        public View<TModel> SetContainerFormat(string format)
        {
            _containerFormat = format;
            return this;
        }

        public View<TModel> SetChildMemberFormat(string format)
        {
            _childMemberFormat = format;
            return this;
        }

        public View<TModel> AlterHtmlFor<T>(Expression<Func<TModel, T>> expression, Action<IHtmlNodeSet> alterRendering)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            _htmlAlterations[propertyName] = alterRendering;
            return this;
        }

        public View<TModel> AlterHtmlWhere(Func<ModelMetadata, bool> predicate, Action<IHtmlNodeSet> alterRendering)
        {
            _conditionalHtmlAlterations.Add(new Tuple<Func<ModelMetadata, bool>, Action<IHtmlNodeSet>>(predicate, alterRendering));
            return this;
        }

        public View<TModel> AlterMetadataFor<T>(Expression<Func<TModel, T>> expression, Action<ModelMetadata> alterMetadata)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            _metadataAlterations[propertyName] = alterMetadata;
            return this;
        }

        public View<TModel> AlterMetadataWhere(Func<ModelMetadata, bool> predicate, Action<ModelMetadata> alterMetadata)
        {
            _conditionalMetadataAlterations.Add(new Tuple<Func<ModelMetadata, bool>, Action<ModelMetadata>>(predicate, alterMetadata));
            return this;
        }

        public string ToHtmlString()
        {
            var modelMetadata = _html.ViewData.ModelMetadata ?? ModelMetadataProviders.Current.GetMetadataForType(() => _model, typeof(TModel));

            using (var writer = new StringWriter())
            {
                foreach (var prop in modelMetadata.Properties)
                {
                    if (_metadataAlterations.ContainsKey(prop.PropertyName))
                    {
                        _metadataAlterations[prop.PropertyName](prop);
                    }

                    foreach (var conditional in _conditionalMetadataAlterations)
                    {
                        if (conditional.Item1(prop))
                        {
                            conditional.Item2(prop);
                        }
                    }

                    if (prop.ShowForDisplay)
                    {
                        var htmlAlterations = new List<Action<IHtmlNodeSet>>();

                        if (_htmlAlterations.ContainsKey(prop.PropertyName))
                        {
                            htmlAlterations.Add(_htmlAlterations[prop.PropertyName]);
                        }

                        foreach (var conditional in _conditionalHtmlAlterations)
                        {
                            if (conditional.Item1(prop))
                            {
                                htmlAlterations.Add(conditional.Item2);
                            }
                        }

                        var htmlString = string.Format(_childMemberFormat,
                            System.Web.Mvc.Html.LabelExtensions.Label(_html, prop.PropertyName),
                            System.Web.Mvc.Html.DisplayExtensions.Display(_html, prop.PropertyName));

                        var doc = new HtmlAgilityPack.HtmlDocument();
                        doc.LoadHtml(htmlString);
                        var htmlNode = new HtmlNode(doc.DocumentNode);

                        foreach (var alteration in htmlAlterations)
                        {
                            alteration(htmlNode);
                        }

                        writer.Write(htmlNode.InnerHtml);
                    }
                }

                return String.Format(_containerFormat, writer.ToString());
            }
        }
    }

    public interface IHtmlNodeSet : IEnumerable<IHtmlNode>
    {
        IHtmlNodeSet AddClass(string className);
        IHtmlNodeSet RemoveClass(string className);
        IHtmlNodeSet SetStyle(string name, string value);
        IHtmlNodeSet RemoveStyle(string name);
        IHtmlNodeSet SetAttribute(string name, string value);
        IHtmlNodeSet RemoveAttribute(string name);
        IHtmlNodeSet SetInnerHtml(string html);
        IHtmlNodeSet Children { get; }
        IHtmlNodeSet Parent { get; }
        IHtmlNodeSet Find(Func<IHtmlNode, bool> predicate);
    }

    public interface IHtmlNode : IHtmlNodeSet
    {
        string TagName { get; }
        IDictionary<string, string> Attributes { get; }
        string InnerHtml { get; }
        string Class { get; }
        IDictionary<string, string> Styles { get; }
        new IHtmlNode Parent { get; }
    }

    public class HtmlNode : IHtmlNode
    {
        internal readonly HtmlAgilityPack.HtmlNode _node;

        public HtmlNode(HtmlAgilityPack.HtmlNode node)
        {
            this._node = node;
        }

        public HtmlNode(string html)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            _node = doc.DocumentNode;
        }

        public string TagName
        {
            get { return _node.Name; }
        }

        public IDictionary<string, string> Attributes
        {
            get { return _node.Attributes.ToDictionary(a => a.Name, a => a.Value); }
        }

        public string InnerHtml
        {
            get { return _node.InnerHtml; }
        }

        public string Class
        {
            get { return _node.GetAttributeValue("class", string.Empty); }
        }

        public IDictionary<string, string> Styles
        {
            get
            {
                return _node.GetAttributeValue("style", string.Empty)
                    .Split(';')
                    .ToDictionary(s => s.Split(':').First().Trim(), s => s.Split(':').Last().Trim());
            }
        }

        public IHtmlNodeSet AddClass(string className)
        {
            var currentClass = _node.GetAttributeValue("class", string.Empty);
            if (currentClass == string.Empty)
            {
                currentClass = className.Trim();
            }
            else
            {
                currentClass += " " + className.Trim();
            }

            _node.SetAttributeValue("class", currentClass);
            return this;
        }

        public IHtmlNodeSet RemoveClass(string className)
        {
            var classes = _node.GetAttributeValue("class", string.Empty)
                .Split(' ')
                .Where(s => s != className);

            if (!classes.Any())
            {
                _node.Attributes.Remove("class");
            }
            else
            {
                _node.SetAttributeValue("class", classes.Aggregate((s1, s2) => s1 + " " + s2));
            }

            return this;
        }

        public IHtmlNodeSet SetStyle(string name, string value)
        {
            var styles = Styles;
            styles[name] = value;
            _node.SetAttributeValue("style", styles.Select(s => s.Key + ": " + s.Value).Aggregate((s1, s2) => s1 + "; " + s2));
            return this;
        }

        public IHtmlNodeSet RemoveStyle(string name)
        {
            var styles = Styles;
            if (styles.ContainsKey(name))
            {
                styles.Remove(name);
                _node.SetAttributeValue("style", styles.Select(s => s.Key + ": " + s.Value).Aggregate((s1, s2) => s1 + "; " + s2));
            }

            return this;
        }

        public IHtmlNodeSet SetAttribute(string name, string value)
        {
            _node.SetAttributeValue(name, value);
            return this;
        }

        public IHtmlNodeSet RemoveAttribute(string name)
        {
            _node.Attributes.Remove(name);
            return this;
        }

        public IHtmlNodeSet SetInnerHtml(string html)
        {
            _node.InnerHtml = html;
            return this;
        }

        public IHtmlNodeSet Children
        {
            get { return new HtmlNodeSet(_node.ChildNodes); }
        }

        public IHtmlNode Parent
        {
            get
            {
                if (_node.Name == "#document")
                {
                    return null;
                }

                return new HtmlNode(_node.ParentNode);
            }
        }

        public IHtmlNodeSet Find(Func<IHtmlNode, bool> predicate)
        {
            return new HtmlNodeSet(this.Children.Where(predicate).Concat(Children.SelectMany(c => c.Find(predicate))));
        }

        IHtmlNodeSet IHtmlNodeSet.Parent
        {
            get { return Parent; }
        }

        public IEnumerator<IHtmlNode> GetEnumerator()
        {
            yield return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            yield return this;
        }
    }

    public class HtmlNodeSet : IHtmlNodeSet
    {
        private readonly List<IHtmlNode> _nodes = new List<IHtmlNode>();

        public HtmlNodeSet(IEnumerable<HtmlAgilityPack.HtmlNode> nodes)
        {
            _nodes.AddRange(nodes.Distinct().Select(n => new HtmlNode(n)));
        }

        public HtmlNodeSet(IEnumerable<IHtmlNode> nodes)
        {
            _nodes.AddRange(nodes);
        }

        public IHtmlNodeSet AddClass(string className)
        {
            foreach (var node in _nodes)
            {
                node.AddClass(className);
            }
            return this;
        }

        public IHtmlNodeSet RemoveClass(string className)
        {
            foreach (var node in _nodes)
            {
                node.RemoveClass(className);
            }
            return this;
        }

        public IHtmlNodeSet SetStyle(string name, string value)
        {
            foreach (var node in _nodes)
            {
                node.SetStyle(name, value);
            }
            return this;
        }

        public IHtmlNodeSet RemoveStyle(string name)
        {
            foreach (var node in _nodes)
            {
                node.RemoveStyle(name);
            }
            return this;
        }

        public IHtmlNodeSet SetAttribute(string name, string value)
        {
            foreach (var node in _nodes)
            {
                node.SetAttribute(name, value);
            }
            return this;
        }

        public IHtmlNodeSet RemoveAttribute(string name)
        {
            foreach (var node in _nodes)
            {
                node.RemoveAttribute(name);
            }
            return this;
        }

        public IHtmlNodeSet SetInnerHtml(string html)
        {
            foreach (var node in _nodes)
            {
                node.SetInnerHtml(html);
            }
            return this;
        }

        public IHtmlNodeSet Children
        {
            get { return new HtmlNodeSet(_nodes.SelectMany(n => ((HtmlNode)n)._node.ChildNodes)); }
        }

        public IHtmlNodeSet Parent
        {
            get { return new HtmlNodeSet(_nodes.Select(n => ((HtmlNode)n.Parent)._node)); }
        }

        public IHtmlNodeSet Find(Func<IHtmlNode, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<IHtmlNode> GetEnumerator()
        {
            return _nodes.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _nodes.GetEnumerator();
        }
    }
}
