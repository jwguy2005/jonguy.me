using CIN.Validation.Errors;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CIN.Fulfillment.AppServices.Core
{
    public class ValidationResult : IEnumerable<ValidationFailure>
    {
        private readonly IEnumerable<ValidationFailure> _items;

        public ValidationResult(IEnumerable<ValidationFailure> items)
        {
            this._items = items;
        }

        public bool IsValid
        {
            get { return !_items.Any(); }
        }

        public static readonly ValidationResult Valid = new ValidationResult(Enumerable.Empty<ValidationFailure>());

        public static ValidationResult Error(string message)
        {
            return new ValidationResult(Enumerable.Repeat(new ValidationFailure(true, message), 1));
        }

        public static ValidationResult From(bool isValid, string errorMessage)
        {
            return isValid ? Valid : Error(errorMessage);
        }

        public IEnumerator<ValidationFailure> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public override string ToString()
        {
            return IsValid ? "Valid" : string.Join(",", _items.Select(e => e.ErrorMessage));
        }

        public static ValidationResult operator &(ValidationResult v1, ValidationResult v2)
        {
            return new ValidationResult(v1.Union(v2));
        }

        public static ValidationResult operator &(ValidationResult v1, IEnumerable<ValidationFailure> v2)
        {
            return new ValidationResult(v1.Union(v2));
        }

        public static ValidationResult operator &(IEnumerable<ValidationFailure> v1, ValidationResult v2)
        {
            return new ValidationResult(v1.Union(v2));
        }
    }

    public static class ValidationExtensions
    {
        public static ValidationResult Assert<T>(this T value, Func<T, bool> assertion, string errorMessage)
        {
            return assertion(value) ?
                ValidationResult.Valid :
                ValidationResult.Error(errorMessage);
        }

        public static ValidationResult MustNotBeNull<T>(this T value, string errorMessage = "Value must not be null")
        {
            return value == null ?
                ValidationResult.Error(errorMessage) :
                ValidationResult.Valid;
        }

        public static ValidationResult MustNotBeNullOrWhitespace(this string s,
            string errorMessage = "Value must not be null or whitespace")
        {
            return string.IsNullOrWhiteSpace(s) ?
                ValidationResult.Error(errorMessage) :
                ValidationResult.Valid;
        }
    }
}
