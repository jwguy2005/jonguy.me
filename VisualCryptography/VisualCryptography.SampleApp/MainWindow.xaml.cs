﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace VisualCryptography.SampleApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var img = DrawText(RandomString(6));
            originalImage.Source = img.ToBitmapSource();

            Bitmap one;
            Bitmap two;
            SplitImage(img, out one, out two);

            imageOne.Source = one.ToBitmapSource();
            imageTwo.Source = two.ToBitmapSource();
            combineOne.Source = one.ToBitmapSource();
            combineTwo.Source = two.ToBitmapSource();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            transform.X = e.NewValue;
        }

        public Bitmap DrawText(string text)
        {
            var font = new Font("Arial", 24);
            var img = new Bitmap(1, 1);
            var drawing = Graphics.FromImage(img);

            var textSize = drawing.MeasureString(text, font);
            img.Dispose();
            drawing.Dispose();

            img = new Bitmap((int)textSize.Width, (int)textSize.Height);
            drawing = Graphics.FromImage(img);
            drawing.Clear(System.Drawing.Color.White);

            var textBrush = new SolidBrush(System.Drawing.Color.Black);
            drawing.DrawString(text, font, textBrush, 0, 0);
            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            return img;
        }

        public void SplitImage(Bitmap source, out Bitmap one, out Bitmap two)
        {
            // get bytes from source image
            var sourceData = source.LockBits(new System.Drawing.Rectangle(0, 0, source.Width, source.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, source.PixelFormat);
            var sourceBytes = new byte[sourceData.Stride * sourceData.Height];
            Marshal.Copy(sourceData.Scan0, sourceBytes, 0, sourceData.Stride * sourceData.Height);

            // lock bits for image one
            one = new Bitmap(source.Width * 2, source.Height * 2, source.PixelFormat);
            var oneData = one.LockBits(new System.Drawing.Rectangle(0, 0, one.Width, one.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, one.PixelFormat);
            var oneBytes = new byte[oneData.Stride * oneData.Height];

            // lock bits for image two
            two = new Bitmap(source.Width * 2, source.Height * 2, source.PixelFormat);
            var twoData = two.LockBits(new System.Drawing.Rectangle(0, 0, two.Width, two.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, two.PixelFormat);
            var twoBytes = new byte[twoData.Stride * twoData.Height];

            var bytesPerPixel = 4;
            for (int y = 0; y < sourceData.Height; y++)
            {
                var rowStart = y * sourceData.Stride;
                for (int x = 0; x < sourceData.Stride; x += bytesPerPixel)
                {
                    var sourceR = sourceBytes[rowStart + x];
                    var sourceG = sourceBytes[rowStart + x + 1];
                    var sourceB = sourceBytes[rowStart + x + 2];
                    var sourceA = sourceBytes[rowStart + x + 3];

                    RgbaSuperPixel pixelOne;
                    RgbaSuperPixel pixelTwo;
                    if (sourceR == 0)
                    {
                        // source pixel is black
                        pixelOne = RgbaSuperPixel.GetRandom();
                        pixelTwo = pixelOne.GetOther();
                    }
                    else
                    {
                        // source pixel is white
                        pixelOne = pixelTwo = RgbaSuperPixel.GetRandom();
                    }

                    int top = oneData.Stride * y * 2 + x * 2;
                    int bottom = top + oneData.Stride;

                    oneBytes[top] = pixelOne.TopLeftR;
                    oneBytes[top + 1] = pixelOne.TopLeftG;
                    oneBytes[top + 2] = pixelOne.TopLeftB;
                    oneBytes[top + 3] = pixelOne.TopLeftA;

                    oneBytes[top + 4] = pixelOne.TopRightR;
                    oneBytes[top + 5] = pixelOne.TopRightG;
                    oneBytes[top + 6] = pixelOne.TopRightB;
                    oneBytes[top + 7] = pixelOne.TopRightA;

                    oneBytes[bottom] = pixelOne.BottomLeftR;
                    oneBytes[bottom + 1] = pixelOne.BottomLeftG;
                    oneBytes[bottom + 2] = pixelOne.BottomLeftB;
                    oneBytes[bottom + 3] = pixelOne.BottomLeftA;

                    oneBytes[bottom + 4] = pixelOne.BottomRightR;
                    oneBytes[bottom + 5] = pixelOne.BottomRightG;
                    oneBytes[bottom + 6] = pixelOne.BottomRightB;
                    oneBytes[bottom + 7] = pixelOne.BottomRightA;

                    twoBytes[top] = pixelTwo.TopLeftR;
                    twoBytes[top + 1] = pixelTwo.TopLeftG;
                    twoBytes[top + 2] = pixelTwo.TopLeftB;
                    twoBytes[top + 3] = pixelTwo.TopLeftA;

                    twoBytes[top + 4] = pixelTwo.TopRightR;
                    twoBytes[top + 5] = pixelTwo.TopRightG;
                    twoBytes[top + 6] = pixelTwo.TopRightB;
                    twoBytes[top + 7] = pixelTwo.TopRightA;

                    twoBytes[bottom] = pixelTwo.BottomLeftR;
                    twoBytes[bottom + 1] = pixelTwo.BottomLeftG;
                    twoBytes[bottom + 2] = pixelTwo.BottomLeftB;
                    twoBytes[bottom + 3] = pixelTwo.BottomLeftA;

                    twoBytes[bottom + 4] = pixelTwo.BottomRightR;
                    twoBytes[bottom + 5] = pixelTwo.BottomRightG;
                    twoBytes[bottom + 6] = pixelTwo.BottomRightB;
                    twoBytes[bottom + 7] = pixelTwo.BottomRightA;
                }
            }

            Marshal.Copy(oneBytes, 0, oneData.Scan0, oneData.Stride * oneData.Height);
            one.UnlockBits(oneData);

            Marshal.Copy(twoBytes, 0, twoData.Scan0, twoData.Stride * twoData.Height);
            two.UnlockBits(twoData);
        }

        private readonly Random _rng = new Random();
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public string RandomString(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
            }

            return new string(buffer);
        }
    }
}
