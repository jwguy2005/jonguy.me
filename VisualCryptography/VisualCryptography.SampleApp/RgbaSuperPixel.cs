﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualCryptography.SampleApp
{
    public class RgbaSuperPixel
    {
        public byte TopLeftR { get; private set; }
        public byte TopLeftG { get; private set; }
        public byte TopLeftB { get; private set; }
        public byte TopLeftA { get; private set; }

        public byte TopRightR { get; private set; }
        public byte TopRightG { get; private set; }
        public byte TopRightB { get; private set; }
        public byte TopRightA { get; private set; }

        public byte BottomLeftR { get; private set; }
        public byte BottomLeftG { get; private set; }
        public byte BottomLeftB { get; private set; }
        public byte BottomLeftA { get; private set; }

        public byte BottomRightR { get; private set; }
        public byte BottomRightG { get; private set; }
        public byte BottomRightB { get; private set; }
        public byte BottomRightA { get; private set; }

        public RgbaSuperPixel GetOther()
        {
            if (this == one)
            {
                return two;
            }
            else
            {
                return one;
            }
        }

        private static RgbaSuperPixel one = new RgbaSuperPixel()
        {
            // black
            TopLeftR = 0,
            TopLeftG = 0,
            TopLeftB = 0,
            TopLeftA = 255,

            // white
            TopRightR = 0,
            TopRightG = 0,
            TopRightB = 0,
            TopRightA = 0,

            // white
            BottomLeftR = 0,
            BottomLeftG = 0,
            BottomLeftB = 0,
            BottomLeftA = 0,

            // black
            BottomRightR = 0,
            BottomRightG = 0,
            BottomRightB = 0,
            BottomRightA = 255
        };

        private static RgbaSuperPixel two = new RgbaSuperPixel()
        {
            // white
            TopLeftR = 0,
            TopLeftG = 0,
            TopLeftB = 0,
            TopLeftA = 0,

            // black
            TopRightR = 0,
            TopRightG = 0,
            TopRightB = 0,
            TopRightA = 255,

            // black
            BottomLeftR = 0,
            BottomLeftG = 0,
            BottomLeftB = 0,
            BottomLeftA = 255,

            // white
            BottomRightR = 0,
            BottomRightG = 0,
            BottomRightB = 0,
            BottomRightA = 0
        };

        public static RgbaSuperPixel GetRandom()
        {
            if (random.Next(0, 2) == 0)
            {
                return one;
            }
            else
            {
                return two;
            }
        }

        private static Random random = new Random();

        private RgbaSuperPixel()
        {

        }
    }
}
