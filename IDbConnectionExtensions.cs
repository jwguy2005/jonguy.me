using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using FastMember;

namespace Reynolds.Cws.Common
{
    public static class IDbConnectionExtensions
    {
        static readonly Dictionary<Type, DbType> typeMap;

        static IDbConnectionExtensions()
        {
            typeMap = new Dictionary<Type, DbType>();
            typeMap[typeof(byte)] = DbType.Byte;
            typeMap[typeof(sbyte)] = DbType.SByte;
            typeMap[typeof(short)] = DbType.Int16;
            typeMap[typeof(ushort)] = DbType.UInt16;
            typeMap[typeof(int)] = DbType.Int32;
            typeMap[typeof(uint)] = DbType.UInt32;
            typeMap[typeof(long)] = DbType.Int64;
            typeMap[typeof(ulong)] = DbType.UInt64;
            typeMap[typeof(float)] = DbType.Single;
            typeMap[typeof(double)] = DbType.Double;
            typeMap[typeof(decimal)] = DbType.Decimal;
            typeMap[typeof(bool)] = DbType.Boolean;
            typeMap[typeof(string)] = DbType.String;
            typeMap[typeof(char)] = DbType.StringFixedLength;
            typeMap[typeof(Guid)] = DbType.Guid;
            typeMap[typeof(DateTime)] = DbType.DateTime;
            typeMap[typeof(DateTimeOffset)] = DbType.DateTimeOffset;
            typeMap[typeof(TimeSpan)] = DbType.Time;
            typeMap[typeof(byte[])] = DbType.Binary;
            typeMap[typeof(byte?)] = DbType.Byte;
            typeMap[typeof(sbyte?)] = DbType.SByte;
            typeMap[typeof(short?)] = DbType.Int16;
            typeMap[typeof(ushort?)] = DbType.UInt16;
            typeMap[typeof(int?)] = DbType.Int32;
            typeMap[typeof(uint?)] = DbType.UInt32;
            typeMap[typeof(long?)] = DbType.Int64;
            typeMap[typeof(ulong?)] = DbType.UInt64;
            typeMap[typeof(float?)] = DbType.Single;
            typeMap[typeof(double?)] = DbType.Double;
            typeMap[typeof(decimal?)] = DbType.Decimal;
            typeMap[typeof(bool?)] = DbType.Boolean;
            typeMap[typeof(char?)] = DbType.StringFixedLength;
            typeMap[typeof(Guid?)] = DbType.Guid;
            typeMap[typeof(DateTime?)] = DbType.DateTime;
            typeMap[typeof(DateTimeOffset?)] = DbType.DateTimeOffset;
            typeMap[typeof(TimeSpan?)] = DbType.Time;
            typeMap[typeof(Object)] = DbType.Object;
        }

        public static IEnumerable<T> Query<T>(this IDbConnection connection, string sql, object parameters = null) where T : class
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = sql;
                
                if (parameters != null)
                {
                    // create command parameters for each property of the supplied object
                    var parameterAccessor = TypeAccessor.Create(parameters.GetType());
                    foreach (var prop in parameterAccessor.GetMembers())
                    {
                        var cmdParam = command.CreateParameter();
                        cmdParam.ParameterName = prop.Name;
                        cmdParam.Value = parameterAccessor[parameters, prop.Name];
                        command.Parameters.Add(cmdParam);
                    }
                }

                // open the connection if it was not already opened.  be a good citizen and
                // return the connection in the same state you found it
                bool iOpenedIt = false;
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                    iOpenedIt = true;
                }

                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Read())
                    {
                        yield break;
                    }

                    // create a mapping between field indices and names, this should help speed things
                    // up when mapping fields in the data reader to properties on the returned entity
                    var fieldIndexMap = new Dictionary<string, int>();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        fieldIndexMap.Add(reader.GetName(i), i);
                    }

                    do
                    {
                        // an entitySet is a collection of 1 or more rows from the data reader that together
                        // represents all of the data necessary to build the object graph of a single item in
                        // the returned result set
                        var entitySet = getNextEntitySet(reader);

                        // create the entity from the entitySet, returning a single instance of T
                        yield return (T)createEntity(typeof(T), "", entitySet, fieldIndexMap);
                    } while (!reader.IsClosed);
                }

                // close the connection if you opened it
                if (iOpenedIt)
                {
                    connection.Close();
                }
            }
        }

        private static IEnumerable<object[]> getNextEntitySet(IDataReader reader)
        {
            if (reader.IsClosed)
            {
                return null;
            }

            // making the assumption that the first column in the data set is unique per entity
            // may want to change this in the future to use some other conventions
            var entityKey = reader.GetValue(0);
            var entitySet = new List<object[]>();
            var stillMoreRecords = false;

            do
            {
                var values = new object[reader.FieldCount];
                reader.GetValues(values);
                entitySet.Add(values);

                stillMoreRecords = reader.Read();
            } while (stillMoreRecords && entityKey.Equals(reader.GetValue(0)));

            if (!stillMoreRecords)
            {
                reader.Close();
            }

            return entitySet;
        }

        private static object createEntity(Type type, string propertyPrefix, IEnumerable<object[]> entitySet, IDictionary<string, int> fieldIndexMap)
        {
            // create an instance of the return type T. this method will generate a type
            // at runtime if T is an interface, otherwise it will try to use a public parameterless
            // constructor to create an instance of the type
            var entity = getInstance(type);

            // create a type accessor, which provides a fast way to access the type's metadata
            // and get/set properties
            var typeAccessor = TypeAccessor.Create(entity.GetType());
            var propertyInfo = getPropertyInfo(type);
            var first = entitySet.First();

            /* This is where things get interesting.
             * 
             * This method should be able to build an object tree, not just a flat object.
             * Column names in the data reader should use property-like syntax
             * to specify parent child relationships.
             * 
             * For example, if a User class has a HomeAddress property
             * of type Address, which in turn has City, State, and Zip properties, the data reader should
             * have columns like: UserID, HomeAddress.City, HomeAddress.State, HomeAddress.Zip.  It is up
             * to whoever supplies the SQL query to get the column naming convention right.
             * 
             * The first pass of this method should bind data fields to properties of the root object
             * in the tree.  This method will then be called recursively to bind the data fields for
             * child objects.
            */ 

            foreach (var field in fieldIndexMap)
            {
                // only worry about fields that should be bound to the current object
                if (field.Key.StartsWith(propertyPrefix))
                {
                    var propertyName = field.Key.Substring(propertyPrefix.Length).Split('.').First();

                    // it is possible that the data field does not actually map to a property on the object,
                    // could be a typo, or just an unnecessary field.  need to check to make sure that the
                    // property actually exists before trying to set it.
                    if (propertyInfo.ContainsKey(propertyName))
                    {
                        var property = propertyInfo[propertyName];

                        // There are three interesting scenarios for a property.  It can be a collection of
                        // items, a child object, or a basic data type
                        if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition().IsAssignableFrom(typeof(IEnumerable<>)))
                        {
                            // this property is a collection of items, so create a list and add any items to it
                            typeAccessor[entity, propertyName] = createList(property.PropertyType.GetGenericArguments()[0], propertyPrefix + propertyName + ".", entitySet, fieldIndexMap);
                        }
                        else if (typeMap.ContainsKey(property.PropertyType))
                        {
                            // if our typeMap contains the type, then it it s basic data type that we can map directly
                            var value = first.GetValue(field.Value);
                            typeAccessor[entity, propertyName] = value == DBNull.Value ? null : value;
                        }
                        else
                        {
                            // this must be a child object, so recursively call the create entity method,
                            // appending this property's name in the property prefix
                            typeAccessor[entity, propertyName] = createEntity(property.PropertyType, propertyPrefix + propertyName + ".", entitySet, fieldIndexMap);
                        }
                    }
                }
            }

            return entity;
        }

        private static object createList(Type type, string propertyPrefix, IEnumerable<object[]> entitySet, IDictionary<string, int> fieldIndexMap)
        {
            // create an instance on the list to add items to
            IList items = (IList)getInstance(typeof(List<>).MakeGenericType(type));

            // find the index of the unique key to identify items in a list.  making the assumption
            // that the first column in the data set that has the given property prefix is unique
            // per list item
            var keyIndex = fieldIndexMap.First(kvp => kvp.Key.StartsWith(propertyPrefix)).Value;

            // use LINQ to group items by key
            foreach (var group in entitySet.Where(r => r.GetValue(keyIndex) != DBNull.Value).GroupBy(r => r.GetValue(keyIndex)))
            {
                // create entity for each group and add it to the list
                items.Add(createEntity(type, propertyPrefix, group, fieldIndexMap));
            }

            return items;
        }

        private static IDictionary<Type, IDictionary<string, PropertyDescriptor>> propertyInfoCache = new Dictionary<Type, IDictionary<string, PropertyDescriptor>>();
        private static IDictionary<string, PropertyDescriptor> getPropertyInfo(Type type)
        {
            // check the cache first, add if necessary
            if (!propertyInfoCache.ContainsKey(type))
            {
                // item is not in cache, get a lock so it can be added
                lock (propertyInfoCache)
                {
                    // check to make sure noone else added the item to the cache 
                    // while waiting for the lock
                    if (!propertyInfoCache.ContainsKey(type))
                    {
                        // if it still isn't there, create and add
                        var targetTypeProps = TypeDescriptor.GetProperties(type);
                        var dict = new Dictionary<string, PropertyDescriptor>();
                        foreach (PropertyDescriptor prop in targetTypeProps)
                        {
                            dict.Add(prop.Name, prop);
                        }

                        propertyInfoCache.Add(type, dict);
                    }
                }
            }

            // return value from cache
            return propertyInfoCache[type];
        }

        private static ModuleBuilder moduleBuilder = null;
        private static Dictionary<Type, Delegate> typeFactories = new Dictionary<Type,Delegate>();
        public static object getInstance(Type type)
        {
            // check the cache first for a factory, add if necessary
            if (!typeFactories.ContainsKey(type))
            {
                // factory is not in cache, get a lock so it can be added
                lock (typeFactories)
                {
                    // check to make sure noone else added the factory while
                    // waiting for the lock
                    if (!typeFactories.ContainsKey(type))
                    {
                        // if the factory is still not in the cache, create and add
                        if (!type.IsInterface)
                        {
                            // check for a parameterless contructor that can be used
                            var constructor = type.GetConstructor(Type.EmptyTypes);
                            if (constructor == null)
                            {
                                throw new Exception("type does not have a parameterless constructor");
                            }

                            // generate a delegate that calls the parameterless constructor
                            var dynamicMethod = new DynamicMethod(type.Name + "_Factory", type, Type.EmptyTypes);
                            var methodIL = dynamicMethod.GetILGenerator();
                            methodIL.Emit(OpCodes.Newobj, constructor);
                            methodIL.Emit(OpCodes.Ret);
                            var del = dynamicMethod.CreateDelegate(typeof(Func<>).MakeGenericType(type));
                            typeFactories.Add(type, del);
                        }
                        else
                        {
                            // if this is an interface, then a concrete type to use needs to be generated
                            if (moduleBuilder == null)
                            {
                                // note: no lock necessary here, since we are already locking above
                                var domain = Thread.GetDomain();
                                var assemblyName = new AssemblyName("DynamicInterfaceImplementations");
                                var assemblyBuilder = domain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
                                moduleBuilder = assemblyBuilder.DefineDynamicModule("DynamicInterfaceImplementations");
                            }

                            var typeBuilder = moduleBuilder.DefineType(type.Name + "_Implementation", TypeAttributes.Public, typeof(object), new[] { type });

                            // for each property on the type, create a public getter and setter
                            foreach (var prop in type.GetProperties())
                            {
                                // this if for the backing field the property exposes
                                var fieldBuilder = typeBuilder.DefineField("_" + prop.Name, prop.PropertyType, FieldAttributes.Private);

                                // this will create the actual property
                                var propertyBuilder = typeBuilder.DefineProperty(prop.Name, System.Reflection.PropertyAttributes.HasDefault, prop.PropertyType, Type.EmptyTypes);

                                // build a get method for the field
                                var getMethodBuilder = typeBuilder.DefineMethod("get_" + prop.Name, MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.HideBySig, prop.PropertyType, Type.EmptyTypes);
                                var getIL = getMethodBuilder.GetILGenerator();
                                getIL.Emit(OpCodes.Ldarg_0);
                                getIL.Emit(OpCodes.Ldfld, fieldBuilder);
                                getIL.Emit(OpCodes.Ret);
                                propertyBuilder.SetGetMethod(getMethodBuilder);

                                // build a set method for the field
                                var setMethodBuilder = typeBuilder.DefineMethod("set_" + prop.Name, MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.HideBySig, null, new[] { prop.PropertyType });
                                var setIL = setMethodBuilder.GetILGenerator();
                                setIL.Emit(OpCodes.Ldarg_0);
                                setIL.Emit(OpCodes.Ldarg_1);
                                setIL.Emit(OpCodes.Stfld, fieldBuilder);
                                setIL.Emit(OpCodes.Ret);
                                propertyBuilder.SetSetMethod(setMethodBuilder);
                            }

                            var generatedType = typeBuilder.CreateType();

                            // get the default parameterless constructor
                            var constructor = generatedType.GetConstructor(Type.EmptyTypes);

                            // create a delegate for this contructor
                            var dynamicMethod = new DynamicMethod(type.Name + "_Factory", type, Type.EmptyTypes);
                            var methodIL = dynamicMethod.GetILGenerator();
                            methodIL.Emit(OpCodes.Newobj, constructor);
                            methodIL.Emit(OpCodes.Ret);
                            var del = dynamicMethod.CreateDelegate(typeof(Func<>).MakeGenericType(type));
                            typeFactories.Add(type, del);
                        }
                    }
                }
            }

            // get the type factory from the cache
            var factory = (Func<object>)typeFactories[type];

            // execute the factory method to create an instance of the type
            return factory();
        }
    }
}
