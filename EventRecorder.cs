using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DomainEvents.Interface
{
    public class EventRecorder
    {
        private readonly EventHub _hub;
        private readonly ICollection<object> _recordedEvents = new Collection<object>();
        private IDisposable _subscription;

        public EventRecorder(EventHub hub)
        {
            this._hub = hub;
        }

        public void StartRecording()
        {
            if (_subscription == null)
            { 
                // subscribe to ALL events
                _subscription = _hub.Subscribe<object>(RecordEvent);
            }
        }

        public void StopRecording()
        {
            if (_subscription != null)
            {
                _subscription.Dispose();
                _subscription = null;
            }
        }

        public void ClearEvents()
        {
            _recordedEvents.Clear();
        }

        public IEnumerable<object> RecordedEvents
        {
            get { return _recordedEvents; }
        }

        private void RecordEvent(object obj)
        {
            _recordedEvents.Add(obj);
        }
    }

    public static class EventRecorderExtensions
    {
        public static EventRecorder StartRecording(this EventHub hub)
        {
            var recorder = new EventRecorder(hub);
            recorder.StartRecording();
            return recorder;
        }

        public static void AssertAtLeastOne<T>(this EventRecorder recorder)
        {
            if (!recorder.RecordedEvents.OfType<T>().Any())
            {
                throw new Exception(string.Format("No events of type {0} were recorded", typeof(T).Name));
            }
        }

        public static void AssertSingle<T>(this EventRecorder recorder)
        {
            var count = recorder.RecordedEvents.OfType<T>().Count();

            if (count == 0)
            {
                throw new Exception(string.Format("No events of type {0} were recorded", typeof(T).Name));
            }

            if (count > 1)
            {
                throw new Exception(string.Format("More than 1 event of type {0} was recorded ({1} were recorded)", typeof(T).Name, count));
            }
        }

        public static void AsertMultiple<T>(this EventRecorder recorder, int expectedCount)
        {
            var count = recorder.RecordedEvents.OfType<T>().Count();

            if (count != expectedCount)
            {
                throw new Exception(string.Format("Unexpected number of events of type {0} were recorded. {1} were expected, but {2} were recorded", typeof(T).Name, expectedCount, count));
            }
        }
    }
}
